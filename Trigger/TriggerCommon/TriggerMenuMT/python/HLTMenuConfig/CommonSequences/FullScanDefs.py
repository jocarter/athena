#  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

""" Provide a place for definitions required by all fullscan signatures """

# RoI used for the full-scan calorimeter step
caloFSRoI = "FSJETMETCaloRoI"
# RoI used for the full-scan tracking step
trkFSRoI = "FSJETMETTrkRoI"
