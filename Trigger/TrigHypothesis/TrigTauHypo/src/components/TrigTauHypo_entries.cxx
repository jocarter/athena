#include "../TrigTauCaloRoiUpdaterMT.h"
#include "../TrigTauGenericHypoMT.h"
#include "../TrigTauCaloHypoAlgMT.h"
#include "../TrigTauTrackRoiUpdaterMT.h"
#include "../TrigTrackPreSelHypoAlgMT.h"
#include "../TrigTrackPreSelHypoTool.h"
#include "../TrigEFTauMVHypoTool.h"
#include "../TrigEFTauMVHypoAlgMT.h"
#include "../TrigTrkPrecHypoAlgMT.h"
#include "../TrigTrkPrecHypoTool.h"
#include "../TrigTauXComboHypoTool.h"
#include "../TrigEFTauDiKaonHypoTool.h"

DECLARE_COMPONENT( TrigTauCaloRoiUpdaterMT )
DECLARE_COMPONENT( TrigTauGenericHypoMT )
DECLARE_COMPONENT( TrigTauCaloHypoAlgMT )
DECLARE_COMPONENT( TrigTauTrackRoiUpdaterMT )
DECLARE_COMPONENT( TrigTrackPreSelHypoAlgMT )
DECLARE_COMPONENT( TrigTrackPreSelHypoTool )
DECLARE_COMPONENT( TrigTrkPrecHypoTool )
DECLARE_COMPONENT( TrigTrkPrecHypoAlgMT )
DECLARE_COMPONENT( TrigEFTauMVHypoAlgMT )
DECLARE_COMPONENT( TrigEFTauMVHypoTool )
DECLARE_COMPONENT( TrigTauXComboHypoTool )
DECLARE_COMPONENT( TrigEFTauDiKaonHypoTool )
